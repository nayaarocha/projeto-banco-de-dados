﻿using MvcApplication1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class ClienteController : Controller
    {
        //
        // GET: /Cliente/

        Cliente cliente = new Cliente();

        public ActionResult Cliente()
        {
            return View(cliente.GetClientes());
        }

        //
        // GET: /Cliente/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }


        //
        // GET: /Cliente/Create
        public ActionResult Create(string Nome,string Endereco, string Cidade,string Estado,string Telefone,string Email, string Sexo)
        {
            return View();
        }

        //
        // POST: /Cliente/Create

        [HttpPost]
        public ActionResult Create(Cliente cliente,FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid) return View();
                cliente.NovoCliente(cliente);
                
                return RedirectToAction("Cliente");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Cliente/Edit/5
        public ActionResult Edit(int id)
        {

            var model = cliente.GetClienteById(id);
            if(model == null) return View("NotFound");
                return View(model);
        }

        //
        // POST: /Cliente/Edit/5
        [HttpPost]
        public ActionResult Edit(Cliente cliente, FormCollection collection)
        {
            if (!ModelState.IsValid) return View();
            cliente.UpdateCliente(cliente);
            return RedirectToAction("Cliente");
        }

        //
        // GET: /Cliente/Delete/5
        public ActionResult Delete(int id)
        {
            var model = cliente.GetClienteById(id);
            if (model == null) return View("NotFound");
            return View(model);
        }

        //
        // POST: /Cliente/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                if (!ModelState.IsValid) return View("notFound");
                cliente.DeleteCliente(id);
                return RedirectToAction("Cliente");
            }
            catch
            {
                return View();
            }
        }



    }
}
